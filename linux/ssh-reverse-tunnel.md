# SSH Reverse Tunnel

## SSH server configuration

```bash
sudo nano /etc/sshd_config
```

Edit sshd_config:

```bash
+ GatewayPorts clientspecified
```

## Remote host configuration

Connect to ssh server:

```bash
ssh -NTR 0.0.0.0:<ssh-server-port>:127.0.0.1:22 <ssh-server-host>
```

### Or add Systemd service

Create service file:

```bash
sudo nano /etc/systemd/system/sshrt.service
```

Add to file:

```
[Unit]
Description=Reverse ssh
After=network.target

[Service]
ExecStart=/usr/bin/ssh -NT -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes -o StrictHostKeyChecking=no -R 0.0.0.0:<ssh-server-port>:127.0.0.1:22 <ssh-server-host>
User=<user>
KillMode=process
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
```
