# NGINX

## Введение

Nginx является одним из самых популярных веб-серверов в мире, его используют для хостинга самых больших и нагруженных сайтов в Интернете. Nginx в подавляющем большинстве случаев менее требователен к ресурсам, чем Apache; его можно использовать как в качестве веб-сервера, так и в качестве обратного прокси-сервера (reverse proxy).

## Documentation

* [nginx (ru)](http://nginx.org/ru/docs/)
* [nginx + pyramid](https://docs.pylonsproject.org/projects/pyramid_cookbook/en/latest/deployment/nginx.html)

## Install nginx

```bash
sudo apt-get update
sudo apt-get install nginx
```

## Check nginx state

```bash
sudo systemctl status nginx
```

## Configure firewall

Open 80, 443 ports.

## Check nginx work

Go to [http://\<server-ip>](). If you see nginx welcome page then nginx succesfuly installed.

## Configure nginx

### Nginx files

#### Логи сервера

* **/etc/nginx/nginx.conf**: основной файл конфигурации Nginx. Этот файл используется для внесения изменений в глобальную конфигурацию Nginx.
* **/var/log/nginx/access.log**: каждый запрос к вашему веб-серверу записывается в этот файл лога, если иное не задано настройками Nginx.
* **/var/log/nginx/error.log**: любые ошибки Nginx будут записываться в этот файл.

**Reload config:**

```bash
sudo systemctl reload nginx
```

Create app config:

```bash
sudo nano etc/nginx/conf.d/<app>.conf
```

Paste to \<app>.conf:

```
upstream <app> {
    server 127.0.0.1:5000;
    server 127.0.0.1:5001;
}

server {
    listen 80;

    # optional ssl configuration

    listen 443 ssl;
    ssl_certificate /path/to/ssl/pem_file;
    ssl_certificate_key /path/to/ssl/certificate_key;

    # end of optional ssl configuration

    server_name  <app-domain>;

    access_log  /home/example/env/access.log;

    location / {
        proxy_set_header        Host $http_host;
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto $scheme;

        client_max_body_size    10m;
        client_body_buffer_size 128k;
        proxy_connect_timeout   60s;
        proxy_send_timeout      90s;
        proxy_read_timeout      90s;
        proxy_buffering         off;
        proxy_temp_file_write_size 64k;
        proxy_pass http://<app>;
        proxy_redirect          off;
    }
}
```
