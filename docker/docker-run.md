# Docker run container

Самый простой способ запустить контейнер:

```bash
    $ docker run <image>
```

Docker создаст контейнер, используюя указанный образ.

Run docker container in detach mode (in background):

```bash
    $ docker run -d <image>
```

Run with virtual tty container:

```bash
    docker run -it <image>
```

Чтобы контейнер удалился после завершения:
```bash
    $ docker run --rm <image>
```

Указать имя контейнера:

```bash
    $ docker run --name <container-name> <image>
```

Пробросить порты контейнера на случайные порты хоста:

```bash
    $ docker run -P <image>
```

Указать перенаправление портов:

```bash
    $ docker run -p <host-port>:<container-port> <image>
```

Указать переменные окружения:

```bash
    $ docker run -e VAR=value <image>
```

или

```bash
    $ docker run -e "VAR=value" <image>
```

Опции добавляющие host в /etc/hosts:

```bash
    --add-host=""
    --add-host="<ip> <host>"
    --add-host="192.168.0.4 node"
    --add-host <host>:<ip>
    --add-host db-static:86.75.30.9
```

## Управление контейнерами

Посмотреть открытые порты у контейнера:

```bash
    $ docker port <container>
```

Остановить контейнер:

```bash
    $ docker stop <container>
```

Завершенные контейнеры занимают место на диске. Для удаления завершенных контейнеров:

```bash
	$ docker rm $(docker ps -a -q -f status=exited)
```



## Образы

Показать образы доступные локально:

```bash
	$ docker images
```

Save tag to remote image:

```bash
    $ docker tag image username/repository:tag
```

### Build image

Build docker image from current dir:

```bash
    $ dicker build -t <tag-name> .
```

Upload image to the cloud:

```bash
    $ docker push username/repository:tag
```

## Use docker hub

Sign up docker account: https://cloud.docker.com

Cli Login:

```bash
    $ docker login
```
