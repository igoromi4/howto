# Docker volumes

Create volume:

```bash
	$ docker volume create <volume-name>
```

Show volumes:

```bash
	$ docker volume ls
```

Show volume details:

```bash
	$ docker volume inspect <volume-name>
```

Use volume:

```bash
	$ docker run -v <volume-name>:<dir>
	$ docker run --volume <volume-name>:<dir>
	$ docker run -v <volume-name>:<dir>:<ro|rw>
	$ docker run --mount source=<volume-name>,tafget=<dir>
```

Remove volume:

```bash
	$ docker volume rm <volume-name>
```
