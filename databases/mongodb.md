# MondoDB

## Links

* [Dot notation](https://docs.mongodb.com/manual/core/document/#document-dot-notation)
* [Replace field](https://docs.mongodb.com/manual/reference/method/db.collection.update/#replace-a-document-entirely)
* [Append Multiple Values to an Array](https://docs.mongodb.com/manual/reference/operator/update/push/#append-multiple-values-to-an-array)
* [Mongo Shell Methods](https://docs.mongodb.com/manual/reference/method)
* [User Management Methods](https://docs.mongodb.com/manual/reference/method/js-user-management)


## Устанавливаем MongoDB

Add GPG key:

```bash
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
```

Add apt repo:

```bash
    echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.4 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
```

Install mongodb:

```bash
    sudo apt-get update
    sudo apt-get install -y mongodb-org

    sudo systemctl start mongod
    mongo
```

## Install Percona Server for MongoDB

**Only on Debian 8!!!**

```bash
wget https://repo.percona.com/apt/percona-release_0.1-4.$(lsb_release -sc)_all.deb
dpkg -i percona-release_0.1-4.$(lsb_release -sc)_all.deb
rm percona-release_0.1-4.$(lsb_release -sc)_all.deb
apt-get update
apt-get install -y percona-server-mongodb-34
```

Start mongo service:

```bash
    sudo service mongod start
    sudo service mongod status
```

Uninstall:

```bash
    sudo service mongod stop
    sudo apt-get remove percona-server-mongodb*
```

## Mongo shell commands

```mongo
	show dbs
    use <db-name>
    show collections
    show db.<collection-name>
```

Update document:

```mongo
db.getCollection('?').update({'user_id':'?'}, {'tx':{'btc':[]}})
```

Поиск и замена:

```mongo
db.api.update( { "carrier.fee": { $gt: 2 } }, { $set: { price: 9.99 } } )
```

Замена для многих документов:

```mongo
db.inventory.update({ "carrier.fee": { $gt: 2 } }, { $set: { price: 9.99 } }, { multi: true });
```
