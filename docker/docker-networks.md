# Docker network

Show networks:

```bash
    $ docker network ls
```

Create networks:

```bash
    $ docker network create <network-name>
```

Run container in network:

```bash
    $ docker run --net <network-name> <image>
```
