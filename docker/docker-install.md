# Install Docker

## Install docker stable version:

```bash
curl -sL https://get.docker.com | sudo bash - 
```

## Install docker test version

**Using these scripts is not recommended for production environments!!!**

```bash
curl -sL https://test.docker.com | sudo bash -
```

## Docker for not root users

```bash
sudo usermod -aG docker $(whoami)
```

## Upgrade docker installed by script

If you installed Docker using the convenience script, you should upgrade Docker using your package manager directly. There is no advantage to re-running the convenience script, and it can cause issues if it attempts to re-add repositories which have already been added to the host machine.

## Uninstall old docker versions

```bash
sudo apt-get remove docker docker-engine docker.io
```
