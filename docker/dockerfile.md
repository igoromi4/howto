# Dockerfile

## Dockerfile Format

```Dockerfile
MAINTAINER <username> <email>

FROM <parent-image>
FROM ubuntu

WORKDIR <workdir>
ADD <host-dir> <container-dir>
ADD . <dir>

RUN <code>
RUN apt-get install curl

EXPOSE <port> # make port avalible outside of container
EXPOSE 80

ENV NAME <value>

CMD <commands>
CMD ["command", "arg"]
```

## Dockerfile example

```Dockerfile
FROM ubuntu

RUN apt-get update && \
    apt-get install -y nodejs

RUN useradd -ms /bin/bash newuser
USER newuser

WORKDIR /home/newuser
COPY . /home/newuser

RUN npm install

EXPOSE 3000

CMD ["node", "/usr/app/app.js"]
```
