# NFS

Network FileSystem

## Install on server

```bash
sudo apt-get install nfs-kernel-server
```

## Install on client

```bash
sudo apt-get install nfs-common
```

## Convigure server

**Open firewall ports tcp/udp 111 and 2049.**

Edit config file:

```bash
sudo nano /etc/exports
```

Add line to config file:

```
<shared-dir> <client-ip>(<options>)
```

Where \<options>:

```
<options> := <option>[,<option>[,...]]
```

Options list:

* ro - read only
* rw - read write
* insecure
* nohide
* all_squash
* anonuid=1000
* anongid=1000
* no_subtree_check


Line example:

```
/data 10.0.0.1(ro,nohide,all_squash)
```

Restart server:

```bash
sudo systemctl restart nfs-server
```

## Convigure client

```bash
sudo mount -t nfs <server-ip>:<shared-dir> <mount-dir>
```

Or add line to /etc/fstab:

```text
<server-ip>:<shared-dir> <mount-dir> nfs user,rw,noauto 0 0
```

### Unmount

On client side:

```bash
sudo umount -k -l <mounted-dir>
```

## Links

* [Ubuntu NFS](http://help.ubuntu.ru/wiki/%D1%80%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE_%D0%BF%D0%BE_ubuntu_server/%D1%84%D0%B0%D0%B9%D0%BB%D0%BE%D0%B2%D1%8B%D0%B5_%D1%81%D0%B5%D1%80%D0%B2%D0%B5%D1%80%D0%B0/nfs)
* [NFSv4Howto](https://help.ubuntu.com/community/NFSv4Howto)
