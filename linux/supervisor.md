# Supervisor

## Install supervisor

```bash
sudo apt-get update
sudo apt-get install supervisor
```

Or with python's pip:

```bash
sudo pip install supervisor
```

## Configure supervisor

* **/etc/supervisor/supervisor.conf** : config file
* **/etc/supervisor/conf.d/\*.conf** : apps config files

## Configure your app

Create config:

```bash
sudo nano /etc/supervisor/conf.d/<app>.conf
```

Paste to \<app>.conf:

```
[program:<app>]
autostart=true
autorestart=true
command=<run-app-command>
process_name=%(program_name)s-%(process_num)01d
user=<user>
numprocs=<process-number>
numprocs_start=0
startsecs=5
redirect_stderr=true
stdout_logfile=<log-file>.log
stderr_logfile=<err-log-file>.log
```

## Control supervisor

Start/stop all process:

```bash
sudo supervisorctl status all
sudo supervisorctl stop all
```

Add/remove process or group:

```bash
sudo supervisorctl add <process-or-group>
sudo supervisorctl remove <process-or-group>
```

Reload configs and add/remove if need:

```bash
sudo supervisorctl update
```

Connect to process in foreground (exit Ctrl+C):

```
sudo supervisorctl fg <process>
```
